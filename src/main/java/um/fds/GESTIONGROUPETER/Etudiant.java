package um.fds.GESTIONGROUPETER;

public class Etudiant extends Personne{
	
	private int numEtudiant;

	public int getNumEtudiant() {
		return numEtudiant;
	}

	public void setNumEtudiant(int numEtudiant) {
		this.numEtudiant = numEtudiant;
	}
	
}
